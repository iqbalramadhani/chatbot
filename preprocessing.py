from numpy.random import randn
from sklearn.feature_extraction.text import TfidfVectorizer
from itertools import combinations
import numpy as np
import re
import sys
import numpy as np
# import pandas as pd
import math
import copy


# vektorizer = TfidfVectorizer(max_df=1.0,min_df=1,norm=None,smooth_idf=True)

class Preprocessing:

  def cesed(self,datas,corpus):
    threshold = 0.6
    rule = []
    terpilih = []
    for idx,data in enumerate(datas):
      rule.append(data)
      pasangan = 0
      if data[1] >= threshold:
        for data1 in datas:
          if data1[0][1] == data[0][1] and data1[1] > data[1]:
            pasangan = 1
            break;
          # endif
        # endfor
        if pasangan == 0:
          rule.append(data)
          terpilih.append(data[0][1])
      # endif
    # endfor
    # print(rule)
    return rule

  def sim(self,corpus):
    # Create the vocabulary.
    vocab = list(set([w for text in corpus for w in text.split(' ')]))
    panjang = len(vocab)
    # print('panjang ',len(vocab))
    # Assign indices to each word.
    word_to_idx = { w: i for i, w in enumerate(vocab) }

    skor1,pangkat1 = self.tf_idf(corpus,panjang,word_to_idx)
    corpus.reverse()
    skor2,pangkat2 = self.tf_idf(corpus,panjang,word_to_idx)
    kali = sum(skor1*skor2)   
    return kali/(math.sqrt(pangkat1)*math.sqrt(pangkat2))

  def tf_idf(self,corpus,panjang,word_to_idx):
    vektorizer = TfidfVectorizer()
    X = vektorizer.fit_transform(corpus)
    feature_name = vektorizer.get_feature_names()
    feature_index = X[0,:].nonzero()[1]
    skor = np.zeros((panjang,1))
    # print(feature_name)
    skor1 = list(zip([feature_name[i] for i in feature_index], [X[0,x] for x in feature_index]))
    # skor = np.array([X[0,x] for x in feature_index])
    for i in skor1:
      skor[word_to_idx[i[0]]] = i[1] 
    # # skor = v[word_to_idx[]]
    pangkat = sum(skor**2)

    return skor,pangkat

  def clean_kata(self,texts):
    newtext = []
    for idx,text in enumerate(texts):
      texts[idx]['pertanyaan'] = re.sub(r'[^\w]', ' ', text['pertanyaan']).lower().strip()
    return texts
  
  def pasangan(self,datas):
    idnya = [idnya['id'] for idnya in datas]
    comb = combinations(idnya, 2) 
    pasangan = [i for i in comb]
    return pasangan
  
  def utama(self,data):
    # print(data)
    # pasangan = self.pasangan(data)
    bersih_kata = self.clean_kata(data)
    nilai_sim = []
    akurasi = 0
    banyak = len(bersih_kata)
    for idx,bk in enumerate(bersih_kata):
      print(idx)
      jawaban = ''
      for bk1 in bersih_kata:
        nilai_sim = self.sim([bk['pertanyaan'],bk1['pertanyaan']])
        if nilai_sim > 0.95:
          jawaban = bk1['id']
          break
      # endfor
      if jawaban == bk['id']:
        akurasi+=1
    # endfor
    print('Akurasi : ',akurasi/banyak)
    # del pasangan,bersih_kata
    # self.cesed(nilai_sim,bersih_kata)
  
  def test_utama(self,data,datauji):
    # print(data)
    # pasangan = self.pasangan(data)
    bersih_kata = self.clean_kata(data)
    bersih_kata1 = re.sub(r'[^\w]', ' ', datauji).lower().strip() 
    akurasi = 0
    banyak = len(bersih_kata)
    # for idx,bk in enumerate(bersih_kata):
    # print(idx)
    jawaban = ''
    for bk1 in bersih_kata:
      nilai_sim = self.sim([bersih_kata1,bk1['pertanyaan']])
      if nilai_sim > 0.7:
        jawaban = bk1['id']
        break
      # endif
    # endfor
    return jawaban