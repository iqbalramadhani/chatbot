import pymysql

class Database:
    def __init__(self):
        host = "db"
        user = "root"
        password = "root"
        db = "chatbot"
        self.con = pymysql.connect(host=host, user=user, password=password, db=db, cursorclass=pymysql.cursors.DictCursor,charset='utf8mb4')
        self.cur = self.con.cursor()

    ######## GET PREDICT DATA ######
    def getData(self,table):
        self.cur.execute("SELECT * FROM "+table+"")
        result = self.cur.fetchall() 
        return result

    def getDataLatih(self,table):
        self.cur.execute("SELECT * FROM "+table+"")
        result = self.cur.fetchall() 
        return result
    
    def getCekData(self,idnya,tabel):
        self.cur.execute("SELECT id,pertanyaan,jawaban FROM "+tabel+" WHERE id = '"+str(idnya)+"'")
        result = self.cur.fetchall() 
        return result
    
    def getDataLatihRand(self,tabel):
        self.cur.execute("SELECT id,pertanyaan FROM "+tabel+" ORDER BY RAND() LIMIT 5")
        result = self.cur.fetchall() 
        return result
    
    def getDataLatihIni(self):
        self.cur.execute("SELECT * FROM data_latih2")
        result = self.cur.fetchall() 
        return result
    
    def getDataUji(self):
        self.cur.execute("SELECT * FROM proses_cosine_4")
        result = self.cur.fetchall() 
        return result
    
    def tambahDataSet(self,data,tabel):
        self.cur.execute("INSERT INTO "+tabel+" (pertanyaan,jawaban) VALUES ('"+data['pertanyaan']+"','"+data['jawaban']+"')")
        self.con.commit()
    
    def ubahDataSet(self,data,tabel,idnya):
        self.cur.execute("UPDATE "+tabel+" SET pertanyaan = '"+data['pertanyaan']+"',jawaban = '"+data['jawaban']+"' WHERE id = "+idnya+"")
        self.con.commit()
    
    def hapusDataSet(self,tabel,idnya):
        self.cur.execute("DELETE FROM "+tabel+" WHERE id = "+idnya+"")
        self.con.commit()