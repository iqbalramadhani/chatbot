CREATE DATABASE chatbot;
use chatbot;

CREATE TABLE `qna` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `pertanyaan` text NOT NULL,
  `jawaban` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

insert  into `qna`(`pertanyaan`,`jawaban`) values 
('hai','hai'),
('hello','ya hallo');