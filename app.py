from flask import Flask,render_template,url_for,request,jsonify,redirect
from flask_cors import CORS,cross_origin
import pymysql
from flask import jsonify
from query import Database
from preprocessing import Preprocessing

preproses = Preprocessing()

app = Flask(__name__)

app = Flask(__name__,template_folder='views',static_folder='static')
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/')
def users():
    db = Database()
    # datanya = db.getData('qna')

    # return jsonify(datanya)
    # return render_template('utama.html')
    return redirect(url_for('chat'))

@app.route("/dataset",methods=['GET','POST'])
def dataset():
  db = Database()
  if request.method == 'POST':
    data = {
      'pertanyaan':request.form['pertanyaan'],
      'jawaban':request.form['jawaban']
    }
    db.tambahDataSet(data,'qna')
    return redirect(url_for('dataset'))
  else:
    test = 'tes data ini'
    datanya = db.getData('qna')
    return render_template('dataset.html',data=datanya)

@app.route("/ubah-dataset",methods=['GET','POST'])
def ubahdataset():
  db = Database()
  if request.method == 'POST':
    data = {
      'pertanyaan':request.form['pertanyaan'],
      'jawaban':request.form['jawaban']
    }
    idnya = request.form['idnya']
    db.ubahDataSet(data,'qna',idnya)
    return redirect(url_for('dataset'))

@app.route("/hapus-dataset",methods=['GET','POST'])
def hapusdataset():
    db = Database()
    idnya = request.args.get("id")    
    db.hapusDataSet('qna',idnya)
    return redirect(url_for('dataset'))

@app.route("/chat",methods=['GET',"POST"])
@cross_origin()
def chat():
  db = Database()
  if request.method == 'POST':
    text = request.json['question']
    datanya = db.getDataLatih('qna')  
    predictedPost = preproses.test_utama(datanya,text)
    if predictedPost != '':
      jawaban = db.getCekData(predictedPost,'qna')[0]['jawaban']
    else:
      jawaban = 'Saya tidak mengerti apa yang anda tanyakan. Bisa ulangi sekali lagi ?'
    return jsonify(jawaban)
  else:
    return render_template('chat.html')

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
