$(document).ready(function () {
  const popup = document.querySelector('.chat-popup');
  const chatBtn = document.querySelector('.chat-btn');
  const submitBtn = document.querySelector('.submit');
  const chatArea = document.querySelector('.chat-area');
  const inputElm = document.querySelector('input');
  const emojiBtn = document.querySelector('#emoji-btn');
  const picker = new EmojiButton();


  // Emoji selection  
  window.addEventListener('DOMContentLoaded', () => {

    picker.on('emoji', emoji => {
      document.querySelector('input').value += emoji;
    });

    emojiBtn.addEventListener('click', () => {
      picker.togglePicker(emojiBtn);
    });
  });

  //   chat button toggler 

  chatBtn.addEventListener('click', () => {
    popup.classList.toggle('show');
  })

  // send msg 
  submitBtn.addEventListener('click', () => {
    // console.log()
    let userInput = $('#chat').val();
    $('#chat').val('');
    console.log("test" + userInput);
    let temp = `<div class="out-msg"><span class="my-msg">${userInput}</span></div>`;
    chatArea.insertAdjacentHTML("beforeend", temp);
    inputElm.value = '';
    $.ajax({
      url: '/chat',
      type: 'POST',
      dataType:'JSON',
      data: JSON.stringify({'question': userInput}),
      contentType: 'application/json;charset=UTF-8',
      success: function (ress) {
        console.log(ress);
        let temp = `<div class="income-msg"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTG6a6KfKK66Jy1eCuDau7yp2rb5dIfGvl45g&usqp=CAU" class="avatar" alt=""><span class="msg">${ress}</span></div>`;
        chatArea.insertAdjacentHTML("beforeend", temp);
        inputElm.value = '';
      },
      error: function (error) {
        console.log(error);
      }
    });
  });

  $('#chat').keyup(function (e) {
    if (e.keyCode == 13) {
      let userInput = $('#chat').val();
      $('#chat').val('');
      console.log("test" + userInput);
      let temp = `<div class="out-msg"><span class="my-msg">${userInput}</span></div>`;
      chatArea.insertAdjacentHTML("beforeend", temp);
      inputElm.value = '';
      $.ajax({
        url: '/chat',
        type: 'POST',
        dataType:'JSON',
        data: JSON.stringify({'question': userInput}),
        contentType: 'application/json;charset=UTF-8',
        success: function (ress) {
          console.log(ress);
          let temp = `<div class="income-msg"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTG6a6KfKK66Jy1eCuDau7yp2rb5dIfGvl45g&usqp=CAU" class="avatar" alt=""><span class="msg">${ress}</span></div>`;
          chatArea.insertAdjacentHTML("beforeend", temp);
          inputElm.value = '';
        },
        error: function (error) {
          console.log(error);
        }
      });
    }
  });

});